﻿using System.Collections;
using UnityEngine;

public class ArrayEx : MonoBehaviour {
	public string[] sArray;

	void Start () {
		sArray = new string[10];
		sArray [0] = "this";
		sArray [1] = "is";
		sArray [2] = "your";
		sArray [3] = "father";
		print ("数组的长度为" + sArray.Length);
		string str="";
		foreach (string sTemp in sArray) {
			str += "  " + sTemp;
		}
		print (str);
	}
}
