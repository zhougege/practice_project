﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loops_foreach : MonoBehaviour {

	// Use this for initialization
	void Start () {
		string str = "hello";
		foreach (char c in str) {
			print (c);
			if (c == 'h') {
				break;
			}
		}
	}

}
