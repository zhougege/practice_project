﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loops_dowhile : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int i = 1;
		do {
			print (i);
			i++;
			if(i==3){
				break;
			}
		} while(i < 6);
	}
	

}
