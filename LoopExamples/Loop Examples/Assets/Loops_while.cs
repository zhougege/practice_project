﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loops_while : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int i = 1;
		while (i < 10) {
			print (i);
			i++;
			if (i == 5) {
				break;
			}
		}
	}
}
